<?php

namespace Project\Editor\Controller\MasterBlock;

use Project\Editor\Model;

class RubricList
{

    /**
     * Возвращает дерево массив рубрик 'Мастер-блоков БР'
     *
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getList()
    {
        return Model\MasterBlock\RubricList::getList();
    }

    /**
     * Возвращает дерево массив рубрик 'Мастер-блоков БР' и мастарблоки
     *
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getListAll()
    {
        return Model\MasterBlock\RubricList::getListAll();
    }

}
<?php

namespace Project\Editor\Controller\MasterBlock;

use Exception;
use Project\Editor\Model;
use Project\Editor\Response;
use Project\Editor\Validator\Form;

class Rubric
{
    const RULES = [
        "PARENT" => 'int',
        "ACTIVE" => 'bool',
        "SORT"   => 'int',
        "NAME"   => [
            'type' => 'string',
            'min'  => 5,
            'max'  => 250,
        ],
    ];

    /**
     * Создает рубрику 'Мастер-блоков БР'
     *
     * @return array $result
     * @throws Exception
     */
    static public function create()
    {
        $data = Form::parseForm(self::RULES);
        $id = Model\MasterBlock\Rubric::create($data);
        return [
            'ID'   => $id,
            'ITEM' => RubricList::getList(),
        ];
    }

    /**
     * Возвращает массив рубрик 'Мастер-блоков БР'
     *
     * @param $id
     *
     * @return array $result
     * @throws Exception
     */
    static public function getById($id)
    {
        $result = Model\MasterBlock\Rubric::getById($id);
        if ($result) {
            return $result;
        } else {
            throw new Exception('Рубрика не найдена', 404);
        }
    }

    /**
     * Обновляет рубрику 'Мастер-блоков БР'
     *
     * @param $id
     *
     * @return array|bool
     * @throws Exception
     */
    static public function update($id)
    {
        $data = Form::parseForm(self::RULES);
        Model\MasterBlock\Rubric::update($id, $data);
        return RubricList::getList();
    }

    /**
     * Удаляет рубрику 'Мастер-блоков БР'
     *
     * @param $id
     *
     * @throws Exception
     */
    static public function delete($id)
    {
        Model\MasterBlock\Rubric::delete($id);
        Response::success();
    }
}
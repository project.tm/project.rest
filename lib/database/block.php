<?php

namespace Project\Editor\Database;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Loader;
use CIBlock;
use Project\Editor\Config;
use Project\Editor\Database\Block\ClientTypeTable;
use Project\Editor\Database\Block\FilialTable;
use Project\Editor\Database\Block\RegionTable;
use Project\Editor\Database\Block\RoleTable;
use Project\Editor\TraitList\Query;
use Project\Editor\Event;
use Megafon\TagCache;

class BlockTable extends DataManager
{

    use Query;

    const PROPS = [
        'REGION'      => RegionTable::class,
        'FILIAL'      => FilialTable::class,
        'ROLE'        => RoleTable::class,
        'CLIENT_TYPE' => ClientTypeTable::class,
    ];

    public static function getTableName()
    {
        return 'be_block';
    }

    /**
     * @return array
     * @throws Main\ArgumentException
     */
    public static function getMap()
    {
        $saveModification = function () {
            return [
                function ($value) {
                    if (!is_array($value)) {
                        return [(int)$value];
                    } else {
                        foreach ($value as $k => $v) {
                            if (empty($v)) {
                                unset($value[$k]);
                            }
                        }
                        return array_map(function ($v) {
                            return (int)$v;
                        }, $value);
                    }
                },
            ];
        };
        $saveValidation = function () {
            return [
                function ($value) {
                    return true;
                    if (is_array($value)) {
                        $result = true;
                        foreach ($value as $item) {
                            if (!(is_numeric($item) and intval($item) > 0)) {
                                $result = false;
                            }
                        }
                        if ($result) {
                            return $result;
                        } else {
                            return 'Некорректный массив с ID элементов ' . print_r($value, true);
                        }

                    } elseif (is_numeric($value) and intval($value) > 0) {
                        return true;

                    } else {
                        return 'Некорректный ID элемента, ожидается целое число или массив целых чисел.' . print_r($value,
                                true);
                    }
                },
            ];
        };
        return [
            'ID'                 => new Entity\IntegerField('ID', [
                'primary'      => true,
                'autocomplete' => true,
            ]),
            'PARENT'             => new Entity\IntegerField('PARENT', [
                'title' => 'Родитель',
            ]),
            'IS_ROOT'            => new Entity\EnumField('IS_ROOT', [
                'values' => [
                    "1",
                    "0",
                ],
            ]),
            'ELEMENT_TYPE'       => new Entity\EnumField('ELEMENT_TYPE', [
                'title'  => 'Тип элемента блока',
                'values' => [
                    "iblock",
                    "master",
                    "template",
                ],
            ]),
            'ELEMENT_ID'         => new Entity\IntegerField('ELEMENT_ID'),
            'PROPS_ID'           => new Entity\IntegerField('PROPS_ID'),
            'SORT'               => new Entity\IntegerField('SORT'),
            'OPEN'               => new Entity\EnumField('OPEN', [
                'values' => [
                    "1",
                    "0",
                ],
            ]),
            'REGION'             => new Entity\IntegerField('REGION', [
                'serialized'             => true,
                'save_data_modification' => $saveModification,
                'validation'             => $saveValidation,
            ]),
            'REGION_FILTER'      => new Main\Entity\ReferenceField(
                'REGION_FILTER',
                RegionTable::class,
                ['=this.ID' => 'ref.BLOCK_ID'],
                ['join_type' => 'INNER']
            ),
            'FILIAL'             => new Entity\IntegerField('FILIAL', [
                'serialized'             => true,
                'save_data_modification' => $saveModification,
                'validation'             => $saveValidation,
            ]),
            'FILIAL_FILTER'      => new Main\Entity\ReferenceField(
                'FILIAL_FILTER',
                FilialTable::class,
                ['=this.ID' => 'ref.BLOCK_ID'],
                ['join_type' => 'INNER']
            ),
            'ROLE'               => new Entity\IntegerField('ROLE', [
                'serialized'             => true,
                'save_data_modification' => $saveModification,
                'validation'             => $saveValidation,
            ]),
            'ROLE_FILTER'        => new Main\Entity\ReferenceField(
                'ROLE_FILTER',
                RoleTable::class,
                ['=this.ID' => 'ref.BLOCK_ID'],
                ['join_type' => 'INNER']
            ),
            'CLIENT_TYPE'        => new Entity\IntegerField('CLIENT_TYPE', [
                'serialized'             => true,
                'save_data_modification' => $saveModification,
                'validation'             => $saveValidation,
            ]),
            'CLIENT_TYPE_FILTER' => new Main\Entity\ReferenceField(
                'CLIENT_TYPE_FILTER',
                ClientTypeTable::class,
                ['=this.ID' => 'ref.BLOCK_ID'],
                ['join_type' => 'INNER']
            ),
            'TYPE'               => new Entity\EnumField('TYPE', [
                'title'  => 'Тип блока',
                'values' => [
                    "block.accordion",
                    "block.files",
                    "block.navigate",
                    "block.news",
                    "block.note",
                    "block.tab",
                    "block.tabs",
                    "block.text",
                    "block.userfiles",
                    "block.xls",
                    "block.master",
                    "block.template",
                ],
            ]),
            'HASH'               => new Entity\StringField('HASH'),
            'MASTER_ID'          => new Entity\IntegerField('MASTER_ID'),
            'HEADER'             => new Entity\StringField('HEADER'),
            'HEADER_SIZE'        => new Entity\IntegerField('HEADER_SIZE'),
            'HEADER_COLOR'       => new Entity\StringField('HEADER_COLOR'),
            'HEADER_IS_SHOW'     => new Entity\EnumField('HEADER_IS_SHOW', [
                'values' => [
                    "1",
                    "0",
                ],
            ]),
            'HEADER_IS_NAV'      => new Entity\EnumField('HEADER_IS_NAV', [
                'values' => [
                    "1",
                    "0",
                ],
            ]),
            'PADDING_BOTTOM'     => new Entity\EnumField('PADDING_BOTTOM', [
                'values' => [
                    "1",
                    "0",
                ],
            ]),
            'BORDER'             => new Entity\EnumField('BORDER', [
                'values' => [
                    "1",
                    "0",
                ],
            ]),
            'ICON'               => new Entity\IntegerField('ICON'),
            'PARAM'              => new Entity\StringField('PARAM'),
            'CONTENT'            => new Entity\StringField('CONTENT'),
        ];
    }

    /**
     * @param array $data
     *
     * @return Entity\AddResult
     * @throws \Exception
     */
    public static function add(array $data)
    {
        $result = parent::add($data);
        if ($result->isSuccess()) {
            RegionTable::startBigData();
            FilialTable::startBigData();
            ClientTypeTable::startBigData();
            RoleTable::startBigData();
            foreach (self::PROPS as $code => $table) {
                if (!empty($data[$code])) {
                    $ID = $result->GetId();
                    foreach (array_unique($data[$code]) as $value) {
                        $table::add([
                            'BLOCK_ID' => $ID,
                            $code      => $value,
                        ]);
                    }
                }
            }
            RegionTable::compileBigData();
            FilialTable::compileBigData();
            ClientTypeTable::compileBigData();
            RoleTable::compileBigData();
        }
        return $result;
    }

    /**
     * @param mixed $primary
     * @param array $data
     *
     * @return Entity\UpdateResult
     * @throws \Exception
     */
    public static function update($primary, array $data)
    {
        $result = parent::update($primary, $data);
        if ($result->isSuccess()) {
            RegionTable::startBigData();
            FilialTable::startBigData();
            ClientTypeTable::startBigData();
            RoleTable::startBigData();
            foreach (self::PROPS as $code => $table) {
                if (isset($data[$code])) {
                    $ID = $result->GetId();
                    $values = array_unique($data[$code]);
                    $values = array_combine($values, $values);
                    $res = $table::getList([
                        'select' => [$code],
                        'filter' => ['BLOCK_ID' => $primary],
                    ]);
                    $delete = [];
                    while ($item = $res->Fetch()) {
                        if (isset($values[$item[$code]])) {
                            unset($values[$item[$code]]);
                        } else {
                            $delete[] = (int)$item[$code];
                        }
                    }
                    if ($delete) {
                        /*
                         * генерируем Where через битрикс
                         */
                        $table::query()->deleteByWhere([
                            'BLOCK_ID' => $ID,
                            $code      => $delete,
                        ]);
                    }
                    foreach ($values as $value) {
                        $table::add([
                            'BLOCK_ID' => $ID,
                            $code      => $value,
                        ]);
                    }
                }
            }
            RegionTable::compileBigData();
            FilialTable::compileBigData();
            ClientTypeTable::compileBigData();
            RoleTable::compileBigData();
        }
        return $result;
    }

    /**
     * @param        $elementId
     * @param string $type
     *
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    public static function deleteByElementType($elementId, $type = Config::BLOCK_TYPE_IBLOCK)
    {
        self::deleteByFilter([
            '=ELEMENT_TYPE' => $type,
            'ELEMENT_ID'    => $elementId,
        ]);

    }

    /**
     * @param $masterId
     *
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    public static function deleteByMasterBlock($masterId)
    {
        self::deleteByFilter([
            '=TYPE'     => Config::DB_BLOCK_MASTER,
            'MASTER_ID' => $masterId,
        ]);
    }

    /**
     * @param $masterId
     *
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    public static function deleteByTemplates($masterId)
    {
        self::deleteByFilter([
            '=TYPE'     => Config::DB_BLOCK_TEMPLATE,
            'MASTER_ID' => $masterId,
        ]);
    }

    /**
     * @param $elementId
     * @param $propsId
     *
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    public static function deleteByElementProps($elementId, $propsId)
    {
        self::deleteByFilter([
            '=ELEMENT_TYPE' => Config::BLOCK_TYPE_IBLOCK,
            'ELEMENT_ID'    => $elementId,
            'PROPS_ID'      => $propsId,
        ]);
    }

    /**
     * @param $filter
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\SystemException
     */
    public static function deleteByFilter($filter)
    {
        $res = self::getList([
            'select' => ['ID', 'ELEMENT_ID', 'TYPE', 'PARAM'],
            'filter' => $filter,
        ]);
        $arDelete = $arElement = $arIblock = [];
        while ($arItem = $res->Fetch()) {
            Event\Block::beforeDelete($arItem);
            $arDelete[] = (int)$arItem['ID'];
            $arElement[] = (int)$arItem['ELEMENT_ID'];
        }
        if ($arDelete) {
            self::query()->deleteByWhere([
                'ID' => $arDelete,
            ]);
        }
        if ($arIblock) {
            $res = ElementTable::getList([
                'select' => [
                    new ExpressionField('IBLOCK', 'DISTINCT %s', 'IBLOCK_ID'),
                ],
                'filter' => [
                    'ID' => $arElement,
                ],
            ]);
            while ($arItem = $res->Fetch()) {
                CIBlock::clearIblockTagCache($arItem['IBLOCK']);
            }
            if (Loader::includeModule('megafon.editor')) {
                foreach ($arElement as $ID) {
                    TagCache\Cache::clearByTag(TagCache\Tags::getIblockElementById($ID));
                }
            }
        }
        return $arElement;
    }

}
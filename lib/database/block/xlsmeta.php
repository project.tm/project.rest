<?php

namespace Project\Editor\Database\Block;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Project\Editor\TraitList\Query;

class XlsMetaTable extends DataManager
{

    use Query;

    public static function getTableName()
    {
        return 'be_block_xls';
    }

    public static function getMap()
    {
        return [
            'BLOCK_ID'  => new Entity\IntegerField('BLOCK_ID'),
            'XLS_ID'    => new Entity\IntegerField('XLS_ID', [
                'primary'  => true,
                'required' => true,
            ]),
            'FILE_NAME' => new Entity\StringField('FILE_NAME'),
            'ROWS'      => new Entity\IntegerField('ROWS'),
            'UNUQUIE'   => new Entity\IntegerField('UNUQUIE'),
            'HEADER'    => new Entity\IntegerField('HEADER'),
        ];
    }

}
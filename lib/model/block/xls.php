<?php

namespace Project\Editor\Model\Block;

use Exception;
use Project\Editor\Database;
use Project\Editor\Model;
use Project\Editor\Helper;

class Xls
{
    /**
     * @param $BLOCK_ID
     *
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     */
    static public function getBlock($BLOCK_ID)
    {
        $arResult = [];
        $arBlock = Helper\Block::getBlockType($BLOCK_ID, 'block.xls');
        $param = $arBlock['PARAM'];
        if (!$param->XLS or !$param->XLS->FILE_ID) {
            throw new Exception('У блока не указан xls файл', 404);
        }
        $arResult['CONFIG'] = $param->XLS;
        $arResult['TABLE'] = Model\Block\Xls::getTable($BLOCK_ID, $param->XLS->FILE_ID);
        return $arResult;
    }

    /**
     * @param $BLOCK_ID
     * @param $XLS_ID
     *
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function getTable($BLOCK_ID, $XLS_ID)
    {
        $arResult = Database\Block\XlsMetaTable::getList([
            'select' => ['BLOCK_ID', 'XLS_ID', 'NAME' => 'FILE_NAME', 'ROWS', 'UNUQUIE', 'HEADER'],
            'filter' => [
                'XLS_ID' => $XLS_ID,
            ],
        ])->Fetch();

        if (empty($arResult)) {
            $arResult = self::upload($BLOCK_ID, $XLS_ID);
            $arResult['HEADER'] = $arResult['DATA'][0];
            unset($arResult['DATA']);
        } else {
            try {
                $arResult['UNUQUIE'] = json_decode($arResult['UNUQUIE']);
            } catch (Exception $e) {
                $arResult['UNUQUIE'] = [];
            }
            try {
                $arResult['HEADER'] = json_decode($arResult['HEADER']);
            } catch (Exception $e) {
                $arResult['HEADER'] = [];
            }
        }

        return $arResult;
    }

    /**
     * @param $BLOCK_ID
     * @param $XLS_ID
     *
     * @return array
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function upload($BLOCK_ID, $XLS_ID)
    {
        Database\Block\XlsDataTable::startBigData();
        $arResult = Model\Xls::getFileById($XLS_ID, function ($rowsId, $arItem) use ($XLS_ID) {
            foreach ($arItem as $coldId => $value) {
                Database\Block\XlsDataTable::add([
                    'XLS_ID'  => $XLS_ID,
                    'ROWS_ID' => $rowsId,
                    'COLD_ID' => $coldId,
                    'VALUE'   => $value,
                ]);
            }
        });
        $arResult['BLOCK_ID'] = $BLOCK_ID;
        Database\Block\XlsMetaTable::add([
            'BLOCK_ID'  => $BLOCK_ID,
            'XLS_ID'    => $XLS_ID,
            'FILE_NAME' => $arResult['NAME'],
            'ROWS'      => $arResult['ROWS'],
            'UNUQUIE'   => json_encode($arResult['UNUQUIE']),
            'HEADER'    => json_encode($arResult['DATA'][0]),
        ]);
        Database\Block\XlsDataTable::compileBigData();
        return $arResult;
    }

}
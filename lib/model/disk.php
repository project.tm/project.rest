<?php

namespace Project\Editor\Model;

use Bitrix\Disk\Driver;
use Bitrix\Disk\Security\FakeSecurityContext;
use Bitrix\Disk\Ui;
use Exception;
use Project\Editor\Config;
use Project\Editor\Helper;

class Disk
{

    /**
     * @param int $FOLDER_ID
     *
     * @return array
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function getList($FOLDER_ID, $arSelect)
    {
        global $USER;
        Helper\Disk::init();
        if (empty($FOLDER_ID)) {
            $FOLDER_ID = Helper\Disk::getRootId();
        }
        $folder = Helper\Disk::getFolder($FOLDER_ID);
        $diskSecurityContext = new FakeSecurityContext($USER);
        $parameters = [
            'select' => $arSelect,
            'filter' => [
                'PARENT_ID' => $folder->getRealObjectId(),
            ],
        ];
        $parameters = Driver::getInstance()->getRightsManager()->addRightsCheck($diskSecurityContext, $parameters,
            ['ID']);
        $arResult = [];
        $res = $folder->getList($parameters);
        while ($row = $res->fetch()) {
            if ($row['TYPE'] == Config::STORAGE_TYPE_FILE) {
                if (isset($row['SIZE'])) {
                    if ($row['SIZE'] > 1000000) {
                        $row['SIZE'] = round($row['SIZE'] / 1000000, 2) . ' МБ';
                    } else {
                        if ($row['SIZE'] > 1000) {
                            $row['SIZE'] = round($row['SIZE'] / 1000) . ' КБ';
                        } else {
                            $row['SIZE'] = $row['SIZE'] . ' Б';
                        }
                    }
                }
            } else {
                unset($row['SIZE']);
            }
            if (isset($row['TIME'])) {
                $row['TIME'] = $row['TIME']->toString();
            }
            $arResult['ITEMS'][] = $row;
        }
        return $arResult;
    }

    /**
     * @param $fileId
     * @param $type
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function delete($fileId, $type)
    {
        Helper\Disk::init();
        switch ($type) {
            case Config::STORAGE_TYPE_FILE:
                $file = Helper\Disk::getFile($fileId);
                if ($file->delete(Helper\User::getId())) {
                    return $file->getParentId();
                }
                throw new Exception('Не удалось удалить файл', 400);
                break;

            case Config::STORAGE_TYPE_FOLDER:
                $folder = Helper\Disk::getFolder($fileId);
                if ($folder->deleteTree(Helper\User::getId())) {
                    return $folder->getParentId();
                }
                throw new Exception('Не удалось удалить папку', 400);
                break;
        }
        throw new Exception('Не верная команда', 400);
    }


    /**
     * @param $FOLDER_ID
     * @param $name
     *
     * @return \Bitrix\Disk\Folder|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function create($FOLDER_ID, $name)
    {
        Helper\Disk::init();
        if (empty($FOLDER_ID)) {
            $FOLDER_ID = Helper\Disk::getRootId();
        }
        if (empty($name)) {
            throw new Exception('Не задано новое имя', 400);
        }
        $folder = Helper\Disk::getFolder($FOLDER_ID);

        $securityContext = $folder->getStorage()->getCurrentUserSecurityContext();
        if (!$folder->canAdd($securityContext)) {
            throw new Exception('Нет прав на создании папки', 401);
        }
        $subFolderModel = $folder->addSubFolder([
            'NAME'       => Ui\Text::correctFolderName($name),
            'CREATED_BY' => Helper\User::getId(),
        ]);
        if ($subFolderModel ===null) {
            throw new Exception(Helper\Error::view($folder->getErrors()), 400);
        }
        return $subFolderModel;
    }

    /**
     * @param $fileId
     * @param $type
     * @param $name
     *
     * @return int
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function rename($fileId, $type, $name)
    {
        Helper\Disk::init();
        if (empty($name)) {
            throw new Exception('Не задано новое имя', 400);
        }
        $file = Helper\Disk::getFileType($fileId, $type);

        $securityContext = $file->getStorage()->getCurrentUserSecurityContext();
        if (!$file->canRename($securityContext)) {
            throw new Exception('Нет прав на переименование папки или файла', 401);
        }
        $subFolderModel = $file->rename(Ui\Text::correctFolderName($name));
        if ($subFolderModel) {
            return $file->getParentId();
        } else {
            throw new Exception(Helper\Error::view($file->getErrors()), 400);
        }
    }

    /**
     * @param $FOLDER_ID
     * @param $arFile
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function upload($FOLDER_ID, $arFile)
    {
        Helper\Disk::init();
        if (empty($FOLDER_ID)) {
            $FOLDER_ID = Helper\Disk::getRootId();
        }
        $folder = Helper\Disk::getFolder($FOLDER_ID);
        if (!$folder->canAdd($folder->getStorage()->getCurrentUserSecurityContext())) {
            throw new Exception('Нет прав на загрузку файла', 401);
        }
        $fileModel = $folder->uploadFile(
            $arFile,
            [
                'NAME'       => $arFile['name'],
                'CREATED_BY' => Helper\User::getId(),
            ]
        );
        if (!$fileModel) {
            throw new Exception(Helper\Error::view($folder->getErrors()), 400);
        }
        return $fileModel->getId();
    }


    /**
     * @param $fileId
     * @param $arFile
     *
     * @return int
     * @throws \Bitrix\Main\NotImplementedException
     */
    static public function updateUpload($fileId, $arFile)
    {
        Helper\Disk::init();
        $fileModel = Helper\Disk::getFile($fileId);
        if (!$fileModel) {
            throw new Exception('Не удалось найти файл.', 404);
        } else {
            if (!$fileModel->canUpdate($fileModel->getStorage()->getCurrentUserSecurityContext())) {
                throw new Exception('Недостаточно прав для добавления файла.', 404);
            } else {
                if (!$fileModel->uploadVersion($arFile, Helper\User::getId())) {
                    throw new Exception('Не удалось обновить файл.', 404);
                }
            }
        }
        return $fileModel->getParentId();
    }

}
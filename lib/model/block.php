<?php

namespace Project\Editor\Model;

use Exception;
use Project\Editor\Config;
use Project\Editor\Convert;
use Project\Editor\Helper;
use Project\Editor\Database\BlockTable;
use Project\Editor\Event;
use Project\Editor\Validator\Form;
use stdClass;

class Block
{

    /**
     * сохранение мастер-блока
     *
     * @param $arTree
     * @param $elementId
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function saveMaster($arTree, $elementId)
    {
        return self::save($arTree, $elementId, Helper\Iblock::getPropertyMasterId());
    }

    /**
     * сохранение шаблона
     *
     * @param $arTree
     * @param $elementId
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function saveTemplate($arTree, $elementId)
    {
        return self::save($arTree, $elementId, Helper\Iblock::getPropertyTemplatesId());
    }

    /**
     * Запись блока
     *
     * @param        $arTree
     * @param        $elementId
     * @param        $propsId
     * @param string $type
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function save($arTree, $elementId, $propsId, $type = Config::BLOCK_TYPE_IBLOCK)
    {
        $res = BlockTable::getList([
            'select' => ['ID', 'TYPE', 'PARAM'],
            'filter' => [
                'ELEMENT_TYPE' => $type,
                'ELEMENT_ID'   => $elementId,
                'PROPS_ID'     => $propsId,
            ],
        ]);
        $arDelete = [];
        while ($arItem = $res->fetch()) {
            try {
                $arDelete[$arItem['ID']] = [
                    'TYPE'  => $arItem['TYPE'],
                    'PARAM' => json_decode($arItem['PARAM']),
                ];
            } catch (Exception $e) {
                $arDelete[$arItem['ID']] = new stdClass();
            }
        }

        /*
         *  связанные таблицы обновляем одним запросом
         */
        $ID = self::saveData($arDelete, $arTree['ID'], $arTree, $type, $elementId, $propsId);
        if ($arDelete) {
            BlockTable::query()->deleteByWhere([
                'ID' => array_keys($arDelete),
            ]);
        }
        return $ID;
    }

    /**
     * Запись всего дерева
     *
     * @param     $arDelete
     * @param     $ID
     * @param     $arTree
     * @param     $type
     * @param     $elementId
     * @param     $propsId
     * @param int $parentId
     *
     * @return int
     * @throws Exception
     */
    private static function saveData(&$arDelete, $ID, $arTree, $type, $elementId, $propsId, $parentId = 0)
    {
        $child = $arTree['CHILD'];
        $hash = isset($arTree['SYSTEM_HASH']) ? $arTree['SYSTEM_HASH'] : false;
        $arTree = Form::parseForm([
            'SORT'           => 'int',
            'OPEN'           => 'bool',
            'TYPE'           => 'string',
            'HASH'           => 'string',
            'HEADER'         => 'string',
            'HEADER_COLOR'   => 'string',
            'HEADER_SIZE'    => 'int',
            'HEADER_IS_SHOW' => 'bool',
            'HEADER_IS_NAV'  => 'bool',
            'PADDING_BOTTOM' => 'bool',
            'BORDER'         => 'bool',
            'REGION'         => 'array-int',
            'FILIAL'         => 'array-int',
            'ROLE'           => 'array-int',
            'CLIENT_TYPE'    => 'array-int',
            'ICON'           => 'int',
            'PARAM'          => 'object',
            'CONTENT'        => 'string',
        ], $arTree);
        $arTree['ELEMENT_TYPE'] = $type;
        $arTree['ELEMENT_ID'] = $elementId;
        $arTree['PROPS_ID'] = $propsId;
        $arTree['PARENT'] = $parentId;
        $arTree['IS_ROOT'] = !$parentId;
        if (isset($arTree['PARAM'])) {
            if (is_string($arTree['PARAM'])) {
                try {
                    $arTree['PARAM'] = json_encode($arTree['PARAM']);
                } catch (Exception $e) {
                    $arTree['PARAM'] = new stdClass();
                }
            }
            if (
                $arTree['TYPE'] === Config::DB_BLOCK_MASTER
                && $arTree['PARAM']->MASTER_BLOCK
                && $arTree['PARAM']->MASTER_BLOCK->ID
            ) {
                $arTree['MASTER_ID'] = (int)$arTree['PARAM']->MASTER_BLOCK->ID;
            }
            if (
                $arTree['TYPE'] === Config::DB_BLOCK_TEMPLATE
                && $arTree['PARAM']->TEMPLATE_BLOCK
                && $arTree['PARAM']->TEMPLATE_BLOCK->ID
            ) {
                $arTree['MASTER_ID'] = (int)$arTree['PARAM']->TEMPLATE_BLOCK->ID;
            }
        }
        Convert\Block::toDb($arTree);
        Event\Block::beforeSave(isset($arDelete[$ID]) ? $arDelete[$ID] : [], $arTree);

        if ($ID > 0) {
            if (isset($arDelete[$ID])) {
                if (empty($hash) || $hash !== Helper\Block::getHash($arTree)) {
                    $result = BlockTable::update($ID, $arTree);
                    if ($result->isSuccess()) {
                        $ID = $result->GetId();
                    }
                }
                unset($arDelete[$ID]);
            } else {
                $ID = 0;
            }
        }
        if ($ID < 1) {
            $result = BlockTable::add($arTree);
            if ($result->isSuccess()) {
                $ID = $result->GetId();
            } else {
                $ID = 0;
            }
        }
        if ($ID) {
            Event\Block::afterSave(isset($arDelete[$ID]) ? $arDelete[$ID] : [], $arTree);
            if ($child and is_array($child)) {
                $isUpdate = false;
                foreach ($child as $sort => $arChildBlocks) {
                    $arChildBlocks['SORT'] = $sort;

                    $newId = self::saveData($arDelete, $arChildBlocks['ID'], $arChildBlocks, $type, $elementId,
                        $propsId, $ID);
                    if ($newId != $arChildBlocks['ID']) {
                        $isUpdate = true;
                        $arTree['CONTENT'] = str_replace('<block id="block-' . $arChildBlocks['ID'] . '"></block>',
                            '<block id="block-' . $newId . '"></block>', $arTree['CONTENT']);
                    }
                }
                if ($isUpdate) {
                    BlockTable::update($ID, [
                        'CONTENT' => $arTree['CONTENT'],
                    ]);
                }
            }
        } else {
            $arDump = [
                'ELEMENT_TYPE' => $type,
                'ELEMENT_ID'   => $elementId,
                'PROPS_ID'     => $propsId,
            ];
            throw new Exception('Не удалось сохранить блок: ' .
                join('; ', $result->getErrorMessages()) .
                PHP_EOL . var_export($arDump, true), 400);
        }
        return $ID;
    }

}
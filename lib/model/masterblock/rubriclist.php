<?php

namespace Project\Editor\Model\MasterBlock;

use CIBlockSection;
use Project\Editor\Model;
use Project\Editor\Helper;

class RubricList
{

    /**
     * текущий инфоблок
     *
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static protected function getIblockId()
    {
        return Helper\Iblock::getMasterBlockId();
    }

    /**
     * получить список для рубрики
     *
     * @param $id
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static protected function getByRubric($id)
    {
        return Model\MasterBlock::getByRubric($id, true);
    }

    /**
     * фильтрация для списка
     *
     * @param $arItem
     *
     * @return array
     */
    static protected function filterItem($arItem)
    {
        return [
            'ID'     => (int)$arItem['ID'],
            'PARENT' => (int)$arItem['IBLOCK_SECTION_ID'],
            'ACTIVE' => $arItem['ACTIVE'] === 'Y',
            'SORT'   => (int)$arItem['SORT'],
            'NAME'   => $arItem['NAME'],
        ];
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getList()
    {
        $arSelect = [
            'ID',
            'IBLOCK_SECTION_ID',
            'ACTIVE',
            'SORT',
            'NAME',
        ];
        $arFilter = ['IBLOCK_ID' => static::getIblockId(), 'ACTIVE' => 'Y'];
        $arRubrics = [];
        $res = CIBlockSection::GetList(['SORT' => 'ASC'], $arFilter, false, $arSelect);
        while ($arItem = $res->Fetch()) {
            $arRubrics[(int)$arItem['IBLOCK_SECTION_ID']][] = static::filterItem($arItem);
        }
        return Helper\Tree::generateTree($arRubrics, 0);
    }

    /**
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getListAll()
    {
        $arSelect = [
            'ID',
            'IBLOCK_SECTION_ID',
            'ACTIVE',
            'SORT',
            'NAME',
        ];
        $arFilter = ['IBLOCK_ID' => static::getIblockId(), 'ACTIVE' => 'Y'];
        $arResult = [];
        $res = CIBlockSection::GetList(['SORT' => 'ASC'], $arFilter, false, $arSelect);
        while ($arItem = $res->Fetch()) {
            $arResult[(int)$arItem['ID']] = static::filterItem($arItem);
        }

        $arRoot = [];
        foreach (static::getByRubric(array_keys($arResult)) as $section => $arData) {
            if ($section) {
                $arResult[$section]['ITEM'] = $arData;
            } else {
                $arRoot = $arData;
            }
        }
        foreach ($arResult as $arItem) {
            $arRubrics[(int)$arItem['PARENT']][$arItem['ID']] = $arItem;
        }
        $arResult = Helper\Tree::generateTree($arRubrics, 0);
        if ($arRoot) {
            $arResult[0] = $arRoot;
        }
        return $arResult;
    }

}
<?php

namespace Project\Editor\Model;

use Bitrix\Main\Loader;
use CMedialib;
use CMedialibCollection;
use CMedialibItem;
use Exception;

class Medialib
{

    /**
     * Чтение папки в медиатеки,
     *
     * @param $name
     * @param $code
     *
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getList($name, $code)
    {
        if (!Loader::IncludeModule('fileman')) {
            throw new Exception('Установите модуль fileman');
        }
        CMedialib::Init();
        $arCollections = CMedialibCollection::GetList([
            'arFilter' => [
                'ACTIVE'      => 'Y',
                'DESCRIPTION' => $code,
            ],
        ]);
        if (empty($arCollections)) {
            $arFields = [
                "arFields" =>
                    [
                        "ID"          => 0,
                        "NAME"        => $name,
                        "DESCRIPTION" => $code,
                        "OWNER_ID"    => 1,
                        "PARENT_ID"   => 0,
                        "KEYWORDS"    => $name,
                        "ACTIVE"      => "Y",
                        "ML_TYPE"     => "1",
                    ],
            ];
            $id = CMedialibCollection::Edit($arFields); // функция возвращает ID коллекции
            if (empty($id)) {
                throw new Exception('В медиатеки не создается папка: ' . $name, 400);
            }
        }
        if (count($arCollections) > 1) {
            throw new Exception('В медиатеке несколько одинаковых папок: ' . $name, 400);
        }
        $arItems = CMedialibItem::GetList(['arCollections' => [$arCollections[0]['ID']]]);
        if (empty($arItems)) {
            throw new Exception('В папке медиатеки "' . $name . '" иконки не найдены', 400);
        } else {
            return $arItems;
        }
    }

}
<?php

use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class megafon_editor extends CModule
{
    public $MODULE_ID = 'project.editor';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->PARTNER_NAME = '';
        $this->PARTNER_URI = '';

        $this->MODULE_NAME = Loc::getMessage('PROJECT_EDITOR_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_EDITOR_MODULE_DESCRIPTION');
    }

    public function InstallDb()
    {
        global $DB;
        $this->UnInstallDb();
        $DB->RunSQLBatch(__DIR__ . '/db/install.sql');
    }

    public function UnInstallDb()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/db/uninstall.sql');
    }

    public function InstallEvents()
    {
        EventManager::getInstance()->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID,
            'Project\Editor\Property\BlockType', 'GetUserTypeDescription');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnBeforeIBlockElementAdd');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnBeforeIBlockElementUpdate');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnAfterIBlockElementUpdate');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnAfterIBlockElementUpdate');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnBeboreIBlockElementDelete', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnBeboreIBlockElementDelete');
        EventManager::getInstance()->registerEventHandler('iblock', 'OnAfterIBlockElementDelete', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnAfterIBlockElementDelete');

        return true;
    }

    public function UnInstallEvents()
    {
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', $this->MODULE_ID,
            'Project\Editor\Property\BlockType', 'GetUserTypeDescription');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementAdd', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnBeforeIBlockElementAdd');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnBeforeIBlockElementUpdate');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnAfterIBlockElementUpdate');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnAfterIBlockElementUpdate');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnBeforeIBlockElementDelete', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnBeforeIBlockElementDelete');
        EventManager::getInstance()->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID,
            'Project\Editor\Event\Iblock', 'OnAfterIBlockElementUpdate');

        return true;
    }

    public function InstallFiles()
    {
        return true;
    }

    public function UnInstallFiles()
    {
        return true;
    }

    public function DoInstall()
    {
        $this->InstallDb();
        $this->InstallFiles();
        $this->InstallEvents();
        ModuleManager::registerModule($this->MODULE_ID);
    }

    public function DoUninstall()
    {
        $this->UnInstallDb();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}